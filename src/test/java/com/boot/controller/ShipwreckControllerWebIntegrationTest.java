package com.boot.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ShipwreckControllerWebIntegrationTest {

	@Autowired
    private TestRestTemplate restTemplate;
	
	@Test
	public void listAll() throws JsonProcessingException, IOException {
		ResponseEntity<String> response = this.restTemplate.getForEntity("/api/v1/shipwrecks", String.class, "");
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = mapper.readTree(response.getBody());
		
		assertThat(json.isMissingNode()).isEqualTo(false);
		assertThat(json.toString()).isEqualTo("[]");
	}
}
