package com.boot.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.boot.model.Shipwreck;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ShipwreckRepositoryIntegrationTest {

	@Autowired
    private TestEntityManager entityManager;

	@Autowired
	private ShipwreckRepository shipwreckRepository;
	
	@Test
	public void findOne() {
		this.entityManager.persist(new Shipwreck(null, "baca", "baca", "best", 90, 88d, 77d, 2016));
		Shipwreck shipwreck = this.shipwreckRepository.findOne(1L);
		assertThat(shipwreck.getName()).isEqualTo("baca");        
	}
	
	@Test
	public void findAll() {
		List<Shipwreck> wrecks = shipwreckRepository.findAll();
		assertEquals(wrecks.size(), 0);
	}
	
}
